package com.example.fintechhw1

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity


class MainActivity : AppCompatActivity() {

    private var mInfoTextView: TextView? = null

    private val request_Code = 42
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mInfoTextView = findViewById(R.id.textView3)

        val button = findViewById<Button>(R.id.button)
            button.setOnClickListener{
            val intent = Intent(this, SecondActivity::class.java)
                startActivityForResult(intent, request_Code)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == request_Code) {
            if (resultCode == RESULT_OK) {
                val returnedResult: String = data?.data.toString()
                mInfoTextView?.text = returnedResult
            }
        }
    }
}


