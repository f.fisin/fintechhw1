package com.example.fintechhw1

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity


class SecondActivity : AppCompatActivity() {


    private var broadcastReceiver: MyBroadcastReceiver? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        broadcastReceiver = MyBroadcastReceiver()

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)



        val button = findViewById<Button>(R.id.button2)
        button.setOnClickListener {
            val intentService = Intent(this, RandomIntentService::class.java)
            Log.i("button2", "button2 clicked")
            startService(
                intentService.putExtra("time", 5)
            );
        }
        val intentFilter = IntentFilter(
            RandomIntentService().ACTION_RANDOMINTENTSERVICE
        )
        intentFilter.addCategory(Intent.CATEGORY_DEFAULT)
        registerReceiver(broadcastReceiver, intentFilter)

    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(broadcastReceiver)
    }


    inner class MyBroadcastReceiver : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent) {
            val result = intent
                .getStringExtra(RandomIntentService().EXTRA_KEY_OUT)
            Log.i("onReceive", "Got it")

            val data = Intent()
            data.data = Uri.parse(result)
            setResult(RESULT_OK, data)
            finish()
        }
    }

}