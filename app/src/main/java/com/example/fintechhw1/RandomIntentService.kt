package com.example.fintechhw1

import android.app.IntentService
import android.content.Intent
import android.util.Log
import java.util.concurrent.TimeUnit


class RandomIntentService : IntentService("myname") {

    val ACTION_RANDOMINTENTSERVICE = "intentservice.RESPONSE"
    val EXTRA_KEY_OUT = "EXTRA_OUT"
    var extraOut = "This received from intent service"

    private val TAG = "IntentServiceLogs"
    override fun onCreate() {
        super.onCreate()
        Log.d(TAG, "onCreate")
    }

    override fun onHandleIntent(intent: Intent?) {
        val tm = intent!!.getIntExtra("time", 0)
        try {
            TimeUnit.SECONDS.sleep(tm.toLong())
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }

        val responseIntent = Intent()

        responseIntent.action = ACTION_RANDOMINTENTSERVICE
        responseIntent.addCategory(Intent.CATEGORY_DEFAULT)
        responseIntent.putExtra(EXTRA_KEY_OUT, extraOut)
        sendBroadcast(responseIntent)


    }
}
